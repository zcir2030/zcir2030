//package agenda.test;
//import agenda.exceptions.InvalidFormatException;
//import agenda.model.base.Activity;
//import agenda.model.base.Contact;
//import agenda.model.repository.classes.RepositoryActivityMock;
//import agenda.model.repository.interfaces.RepositoryActivity;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.Test;
//
//import java.util.Calendar;
//import java.util.Date;
//import java.util.LinkedList;
//import java.util.List;
//
//import static org.junit.Assert.assertTrue;
//
//public class TestBlackWhite {
//
//    private static RepositoryActivity rep;
//
//    @BeforeAll
//    static void before()
//    {
//        rep = new RepositoryActivityMock();
//
//        String dateS = "12/12/2000";
//        String timeS = "20:00";
//        Calendar c = Calendar.getInstance();
//        c.set(Integer.parseInt(dateS.split("/")[2]),
//                Integer.parseInt(dateS.split("/")[0]) - 1,
//                Integer.parseInt(dateS.split("/")[1]),
//                Integer.parseInt(timeS.split(":")[0]),
//                Integer.parseInt(timeS.split(":")[1]));
//        Date start = c.getTime();
//
//        String dateE = "13/12/2000";
//        String timeE = "20:00";
//
//        c.set(Integer.parseInt(dateE.split("/")[2]),
//                Integer.parseInt(dateE.split("/")[0]) - 1,
//                Integer.parseInt(dateE.split("/")[1]),
//                Integer.parseInt(timeE.split(":")[0]),
//                Integer.parseInt(timeE.split(":")[1]));
//        Date end = c.getTime();
//
//        Activity act = new Activity("1", start, end,
//                new LinkedList<Contact>(), "1", "location1");
//        rep.addActivity(act);
//
//
//        String dateS2 = "13/12/2000";
//        String timeS2 = "20:00";
//        Calendar c2 = Calendar.getInstance();
//        c2.set(Integer.parseInt(dateS2.split("/")[2]),
//                Integer.parseInt(dateS2.split("/")[0]) - 1,
//                Integer.parseInt(dateS2.split("/")[1]),
//                Integer.parseInt(timeS2.split(":")[0]),
//                Integer.parseInt(timeS2.split(":")[1]));
//        Date start2 = c2.getTime();
//
//        String dateE2 = "14/12/2000";
//        String timeE2 = "20:00";
//
//        c2.set(Integer.parseInt(dateE2.split("/")[2]),
//                Integer.parseInt(dateE2.split("/")[0]) - 1,
//                Integer.parseInt(dateE2.split("/")[1]),
//                Integer.parseInt(timeE2.split(":")[0]),
//                Integer.parseInt(timeE2.split(":")[1]));
//        Date end2 = c2.getTime();
//
//        Activity act2 = new Activity("1", start2, end2,
//                new LinkedList<Contact>(), "1", "location1");
//        rep.addActivity(act2);
//
//    }
//
//    @Test
//    public void testWhite()
//    {
//        String dateS = "13/12/2000";
//        Calendar c = Calendar.getInstance();
//
//        c.set(Integer.parseInt(dateS.split("/")[2]),
//                Integer.parseInt(dateS.split("/")[0]) - 1,
//                Integer.parseInt(dateS.split("/")[1]));
//        Date d = c.getTime();
//
//        List<Activity> result = rep.activitiesOnDate("1", d);
//        assertTrue(result.size() == 1);
//    }
//
//    @Test
//    public void testBlack()
//    {
//        String dateS = "10/12/2003";
//        Calendar c = Calendar.getInstance();
//
//        c.set(Integer.parseInt(dateS.split("/")[2]),
//                Integer.parseInt(dateS.split("/")[0]) - 1,
//                Integer.parseInt(dateS.split("/")[1]));
//        Date d = c.getTime();
//
//        List<Activity> result = rep.activitiesOnDate("1", d);
//        assertTrue(result.size() == 0);
//    }
//}
