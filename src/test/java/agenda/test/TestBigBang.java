package agenda.test;
import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryActivityMock;
import agenda.model.repository.classes.RepositoryContactMock;
import agenda.model.repository.interfaces.RepositoryActivity;

import agenda.model.repository.interfaces.RepositoryContact;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

public class TestBigBang {
    private static RepositoryActivity repAct;
    private static RepositoryContact repCont;

    @BeforeAll
    static void before()
    {
        repCont = new RepositoryContactMock();
        repAct = new RepositoryActivityMock();
    }

    public boolean testA()
    {
        try {
            Contact c = new Contact("name", "address1", "0722412918","aaaa");
            repCont.addContact(c);
            return repCont.count() >= 1;
        } catch (InvalidFormatException e) {
            fail("1");
            return false;
        }
    }


    public boolean testB()
    {
        try {
            String dateS = "05/05/2010";
            String timeS = "19:00";
            Calendar c = Calendar.getInstance();
            c.set(Integer.parseInt(dateS.split("/")[2]),
                    Integer.parseInt(dateS.split("/")[0]) - 1,
                    Integer.parseInt(dateS.split("/")[1]),
                    Integer.parseInt(timeS.split(":")[0]),
                    Integer.parseInt(timeS.split(":")[1]));
            Date start = c.getTime();

            String dateE = "05/05/2010";
            String timeE = "22:00";

            c.set(Integer.parseInt(dateE.split("/")[2]),
                    Integer.parseInt(dateE.split("/")[0]) - 1,
                    Integer.parseInt(dateE.split("/")[1]),
                    Integer.parseInt(timeE.split(":")[0]),
                    Integer.parseInt(timeE.split(":")[1]));
            Date end = c.getTime();

            Activity act = new Activity("1", start, end,
                    new LinkedList<Contact>(), "1", "location");
            return repAct.addActivity(act);

        } catch (Exception e) {
            fail("2");
            return false;
        }
    }


    public boolean testC()
    {
        String dateS = "05/05/2010";
        Calendar c = Calendar.getInstance();

        c.set(Integer.parseInt(dateS.split("/")[2]),
                Integer.parseInt(dateS.split("/")[0]) - 1,
                Integer.parseInt(dateS.split("/")[1]));
        Date d = c.getTime();

        List<Activity> result = repAct.activitiesOnDate("1", d);
        System.out.println(result);
        return result.size() == 0;
    }

    @Test
    public void testP()
    {
        boolean t1 = testA();
        boolean t2 = testB();
        boolean t3 = testC();
        assertTrue(t1 && t2 && t3);
    }
}
