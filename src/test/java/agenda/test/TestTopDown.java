package agenda.test;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryActivityMock;
import agenda.model.repository.classes.RepositoryContactMock;
import agenda.model.repository.interfaces.RepositoryActivity;
import agenda.model.repository.interfaces.RepositoryContact;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TestTopDown {

    private static RepositoryActivity repAct;
    private static RepositoryContact repCont;

    @BeforeAll
    static void before()
    {
        repCont = new RepositoryContactMock();
        repAct = new RepositoryActivityMock();
    }

    @Test
    public void testA()
    {
        try {
            Contact c = new Contact("NAME1", "address1", "0722412918","aaaa");
            repCont.addContact(c);
            assertTrue(repCont.count() >= 1);
        } catch (InvalidFormatException e) {
            fail("1");
        }
    }

    @Test
    public void testPAB()
    {
        Contact cont = null;
        try {
            cont = new Contact("NAME2", "address1", "0722412918","aaaa");
            repCont.addContact(cont);
            assertTrue(repCont.count() >= 1);
        } catch (InvalidFormatException e) {
            fail("1");
            e.printStackTrace();
        }

        try {
            String dateS = "05/05/2010";
            String timeS = "19:00";
            Calendar c = Calendar.getInstance();
            c.set(Integer.parseInt(dateS.split("/")[2]),
                    Integer.parseInt(dateS.split("/")[0]) - 1,
                    Integer.parseInt(dateS.split("/")[1]),
                    Integer.parseInt(timeS.split(":")[0]),
                    Integer.parseInt(timeS.split(":")[1]));
            Date start = c.getTime();

            String dateE = "05/05/2010";
            String timeE = "22:00";

            c.set(Integer.parseInt(dateE.split("/")[2]),
                    Integer.parseInt(dateE.split("/")[0]) - 1,
                    Integer.parseInt(dateE.split("/")[1]),
                    Integer.parseInt(timeE.split(":")[0]),
                    Integer.parseInt(timeE.split(":")[1]));
            Date end = c.getTime();

            LinkedList<Contact> newList = new LinkedList<Contact>();
            newList.add(cont);

            Activity act = new Activity("1", start, end, newList, "1", "location");
            assertTrue(repAct.addActivity(act));

        } catch (Exception e) {
            fail("2");
        }
    }

    @Test
    public void testPABC()
    {
        Contact cont = null;
        try {
            cont = new Contact("NAME3", "address1", "0722412918","aaaa");
            repCont.addContact(cont);
            assertTrue(repCont.count() >= 1);
        } catch (InvalidFormatException e) {
            fail("1");
            e.printStackTrace();
        }

        try {
            String dateS = "06/06/2010";
            String timeS = "19:00";
            Calendar c = Calendar.getInstance();
            c.set(Integer.parseInt(dateS.split("/")[2]),
                    Integer.parseInt(dateS.split("/")[0]) - 1,
                    Integer.parseInt(dateS.split("/")[1]),
                    Integer.parseInt(timeS.split(":")[0]),
                    Integer.parseInt(timeS.split(":")[1]));
            Date start = c.getTime();

            String dateE = "06/06/2010";
            String timeE = "22:00";

            c.set(Integer.parseInt(dateE.split("/")[2]),
                    Integer.parseInt(dateE.split("/")[0]) - 1,
                    Integer.parseInt(dateE.split("/")[1]),
                    Integer.parseInt(timeE.split(":")[0]),
                    Integer.parseInt(timeE.split(":")[1]));
            Date end = c.getTime();

            LinkedList<Contact> newList = new LinkedList<Contact>();
            newList.add(cont);

            Activity act = new Activity("1", start, end, newList, "1", "location");
            assertTrue(repAct.addActivity(act));

        } catch (Exception e) {
            fail("2");
        }

        String dateS = "06/06/2010";
        Calendar c = Calendar.getInstance();

        c.set(Integer.parseInt(dateS.split("/")[2]),
                Integer.parseInt(dateS.split("/")[0]) - 1,
                Integer.parseInt(dateS.split("/")[1]));
        Date d = c.getTime();

        List<Activity> result = repAct.activitiesOnDate("1", d);
        System.out.println(result);
        assertEquals(0, result.size());
    }
}
