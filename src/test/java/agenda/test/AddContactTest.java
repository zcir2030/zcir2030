package agenda.test;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import agenda.exceptions.InvalidFormatException;

import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryContactMock;
import agenda.model.repository.interfaces.RepositoryContact;
import org.junit.rules.ExpectedException;


public class AddContactTest {

	private Contact con;
	private RepositoryContact rep;

	public AddContactTest() throws InvalidFormatException {
	}

	@Before
	public void setUp() throws Exception {
		rep = new RepositoryContactMock();
	}
	
	@Test
	public void ABCtest()
	{
		try {
			con = new Contact("name1", "address1", "+4071122334455", "email@domain.com");
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		//int n = rep.count();
		rep.addContact(con);
		for(Contact c : rep.getContacts())
			if (c.equals(con))
			{
				assertTrue(true);
				break;
			}
		//assertTrue(n+1 == rep.count());
	}
	
	@Test
	public void test2()
	{
		try{
			rep.addContact((Contact) new Object());
		}
		catch(Exception e)
		{
			assertTrue(true);
		}	
	}
	
	@Test
	public void test3()
	{
		
		try {
			con = new Contact("name", "address1", "+071122334455", "email@domain.com");
			rep.addContact(con);
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		int n  = rep.count();
		if (n == 1) 
			if (con.equals(rep.getContacts().get(0))) assertTrue(true);
			else assertTrue(false);
	}
	@Test
	public void DEFtest()
	{

		try {
			con = new Contact("1223name", "address1", "+071122334455", "email@domain.com");
			rep.addContact(con);
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		int n  = rep.count();
		if (n == 1)
			if (con.equals(rep.getContacts().get(0))) assertTrue(true);
			else assertTrue(false);
	}

	@Test
	public void test5()
	{

		try {
			con = new Contact("Catalina", "address1", "+071122334455111111", "email@domain.com");
			rep.addContact(con);
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		int n  = rep.count();
		if (n == 1)
			if (con.equals(rep.getContacts().get(0))) assertTrue(true);
			else assertTrue(false);
	}

	@Test
	public void GHItest()
	{
		try {
			con = new Contact("Zugravu", "Cluj-Napoca", "+40721356784", "email@domain.com");
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		rep.addContact(con);
		for(Contact c : rep.getContacts())
			if (c.equals(con)) {
				assertTrue(true);
				break;
			}
	}

	@Test
	public void JKLtest()
	{

		try {
			con = new Contact("Catalina11111", "address1", "+071122334455", "email@domain.com");
			rep.addContact(con);
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		int n  = rep.count();
		if (n == 1)
			if (con.equals(rep.getContacts().get(0))) assertTrue(true);
			else assertTrue(false);
	}

	@Test
	public void test8()
	{

		try {
			con = new Contact("Catalina", "address1", "+07114455aa", "email@domain.com");
			rep.addContact(con);
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		int n  = rep.count();
		if (n == 1)
			if (con.equals(rep.getContacts().get(0))) assertTrue(true);
			else assertTrue(false);
	}

	@Test
	public void test9()
	{

		try {
			con = new Contact("Catalina", "address1", "aaaaaaaa", "email@domain.com");
			rep.addContact(con);
		} catch (InvalidFormatException e) {
			assertFalse(false);
		}
		int n  = rep.count();
		if (n == 1)
			if (con.equals(rep.getContacts().get(0))) assertTrue(true);
			else assertTrue(false);
	}

	@Test
	public void test10()
	{

		try {
			con = new Contact("Andreea", "Neamt", "+40725641458", "email@domain.com");
			rep.addContact(con);
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		int n  = rep.count();
		if (n == 1)
			if (con.equals(rep.getContacts().get(0))) assertTrue(true);
			else assertTrue(false);
	}
}
